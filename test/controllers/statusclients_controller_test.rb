require 'test_helper'

class StatusclientsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @statusclient = statusclients(:one)
  end

  test "should get index" do
    get statusclients_url
    assert_response :success
  end

  test "should get new" do
    get new_statusclient_url
    assert_response :success
  end

  test "should create statusclient" do
    assert_difference('Statusclient.count') do
      post statusclients_url, params: { statusclient: { statut: @statusclient.statut } }
    end

    assert_redirected_to statusclient_url(Statusclient.last)
  end

  test "should show statusclient" do
    get statusclient_url(@statusclient)
    assert_response :success
  end

  test "should get edit" do
    get edit_statusclient_url(@statusclient)
    assert_response :success
  end

  test "should update statusclient" do
    patch statusclient_url(@statusclient), params: { statusclient: { statut: @statusclient.statut } }
    assert_redirected_to statusclient_url(@statusclient)
  end

  test "should destroy statusclient" do
    assert_difference('Statusclient.count', -1) do
      delete statusclient_url(@statusclient)
    end

    assert_redirected_to statusclients_url
  end
end
