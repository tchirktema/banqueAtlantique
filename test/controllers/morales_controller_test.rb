require 'test_helper'

class MoralesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @morale = morales(:one)
  end

  test "should get index" do
    get morales_url
    assert_response :success
  end

  test "should get new" do
    get new_morale_url
    assert_response :success
  end

  test "should create morale" do
    assert_difference('Morale.count') do
      post morales_url, params: { morale: { actionnaires: @morale.actionnaires, adresse: @morale.adresse, adresse_comp_nom: @morale.adresse_comp_nom, adresse_nom: @morale.adresse_nom, affichage_sld_si_cond: @morale.affichage_sld_si_cond, agec: @morale.agec, agence: @morale.agence, aml: @morale.aml, apparente: @morale.apparente, assujetissement_tva: @morale.assujetissement_tva, autorisation_chequier: @morale.autorisation_chequier, bic: @morale.bic, ca_social_ht: @morale.ca_social_ht, calcul_de_performance: @morale.calcul_de_performance, capital: @morale.capital, categ_jur: @morale.categ_jur, classification: @morale.classification, code_ape: @morale.code_ape, code_postal: @morale.code_postal, consolide: @morale.consolide, contrat_gestion: @morale.contrat_gestion, courr: @morale.courr, credit_immed_chq_effets: @morale.credit_immed_chq_effets, date: @morale.date, date_decision: @morale.date_decision, date_echance: @morale.date_echance, date_ouv: @morale.date_ouv, date_ouverture: @morale.date_ouverture, dateffet: @morale.dateffet, devise_de_reference: @morale.devise_de_reference, effect: @morale.effect, email: @morale.email, expl_resp: @morale.expl_resp, fatca: @morale.fatca, fax: @morale.fax, fixe: @morale.fixe, forme: @morale.forme, forme_juridique: @morale.forme_juridique, groupe_activite: @morale.groupe_activite, groupe_de_clients_lies: @morale.groupe_de_clients_lies, grp_confid_client: @morale.grp_confid_client, ident: @morale.ident, instruction_tel: @morale.instruction_tel, interdiction_chequier: @morale.interdiction_chequier, iub_cip: @morale.iub_cip, langue: @morale.langue, langue: @morale.langue, lieu: @morale.lieu, mont_max: @morale.mont_max, nature_clientele: @morale.nature_clientele, ninea: @morale.ninea, nom_jur: @morale.nom_jur, nom_signataire: @morale.nom_signataire, notation_type: @morale.notation_type, num_ifu: @morale.num_ifu, obj_sco: @morale.obj_sco, option_fiscal: @morale.option_fiscal, participatio_banque_dans_client: @morale.participatio_banque_dans_client, participation_client_dans_banque: @morale.participation_client_dans_banque, pays: @morale.pays, pays_crea: @morale.pays_crea, pays_de_nationalite: @morale.pays_de_nationalite, pays_de_residence: @morale.pays_de_residence, pays_de_residence_fiscal: @morale.pays_de_residence_fiscal, pays_exploit: @morale.pays_exploit, pouvoir: @morale.pouvoir, prs: @morale.prs, public_privee: @morale.public_privee, qualite_fonction: @morale.qualite_fonction, racine_cli: @morale.racine_cli, rcs: @morale.rcs, releves_mois_mvts: @morale.releves_mois_mvts, releves_mois_sans_mvts: @morale.releves_mois_sans_mvts, secret: @morale.secret, secteur_activite: @morale.secteur_activite, secteur_institutionnel: @morale.secteur_institutionnel, segmentation: @morale.segmentation, siren: @morale.siren, sres: @morale.sres, sucession: @morale.sucession, surveillance: @morale.surveillance, type_signataire: @morale.type_signataire, ville: @morale.ville } }
    end

    assert_redirected_to morale_url(Morale.last)
  end

  test "should show morale" do
    get morale_url(@morale)
    assert_response :success
  end

  test "should get edit" do
    get edit_morale_url(@morale)
    assert_response :success
  end

  test "should update morale" do
    patch morale_url(@morale), params: { morale: { actionnaires: @morale.actionnaires, adresse: @morale.adresse, adresse_comp_nom: @morale.adresse_comp_nom, adresse_nom: @morale.adresse_nom, affichage_sld_si_cond: @morale.affichage_sld_si_cond, agec: @morale.agec, agence: @morale.agence, aml: @morale.aml, apparente: @morale.apparente, assujetissement_tva: @morale.assujetissement_tva, autorisation_chequier: @morale.autorisation_chequier, bic: @morale.bic, ca_social_ht: @morale.ca_social_ht, calcul_de_performance: @morale.calcul_de_performance, capital: @morale.capital, categ_jur: @morale.categ_jur, classification: @morale.classification, code_ape: @morale.code_ape, code_postal: @morale.code_postal, consolide: @morale.consolide, contrat_gestion: @morale.contrat_gestion, courr: @morale.courr, credit_immed_chq_effets: @morale.credit_immed_chq_effets, date: @morale.date, date_decision: @morale.date_decision, date_echance: @morale.date_echance, date_ouv: @morale.date_ouv, date_ouverture: @morale.date_ouverture, dateffet: @morale.dateffet, devise_de_reference: @morale.devise_de_reference, effect: @morale.effect, email: @morale.email, expl_resp: @morale.expl_resp, fatca: @morale.fatca, fax: @morale.fax, fixe: @morale.fixe, forme: @morale.forme, forme_juridique: @morale.forme_juridique, groupe_activite: @morale.groupe_activite, groupe_de_clients_lies: @morale.groupe_de_clients_lies, grp_confid_client: @morale.grp_confid_client, ident: @morale.ident, instruction_tel: @morale.instruction_tel, interdiction_chequier: @morale.interdiction_chequier, iub_cip: @morale.iub_cip, langue: @morale.langue, langue: @morale.langue, lieu: @morale.lieu, mont_max: @morale.mont_max, nature_clientele: @morale.nature_clientele, ninea: @morale.ninea, nom_jur: @morale.nom_jur, nom_signataire: @morale.nom_signataire, notation_type: @morale.notation_type, num_ifu: @morale.num_ifu, obj_sco: @morale.obj_sco, option_fiscal: @morale.option_fiscal, participatio_banque_dans_client: @morale.participatio_banque_dans_client, participation_client_dans_banque: @morale.participation_client_dans_banque, pays: @morale.pays, pays_crea: @morale.pays_crea, pays_de_nationalite: @morale.pays_de_nationalite, pays_de_residence: @morale.pays_de_residence, pays_de_residence_fiscal: @morale.pays_de_residence_fiscal, pays_exploit: @morale.pays_exploit, pouvoir: @morale.pouvoir, prs: @morale.prs, public_privee: @morale.public_privee, qualite_fonction: @morale.qualite_fonction, racine_cli: @morale.racine_cli, rcs: @morale.rcs, releves_mois_mvts: @morale.releves_mois_mvts, releves_mois_sans_mvts: @morale.releves_mois_sans_mvts, secret: @morale.secret, secteur_activite: @morale.secteur_activite, secteur_institutionnel: @morale.secteur_institutionnel, segmentation: @morale.segmentation, siren: @morale.siren, sres: @morale.sres, sucession: @morale.sucession, surveillance: @morale.surveillance, type_signataire: @morale.type_signataire, ville: @morale.ville } }
    assert_redirected_to morale_url(@morale)
  end

  test "should destroy morale" do
    assert_difference('Morale.count', -1) do
      delete morale_url(@morale)
    end

    assert_redirected_to morales_url
  end
end
