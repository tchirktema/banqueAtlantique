require 'test_helper'

class AgencesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @agence = agences(:one)
  end

  test "should get index" do
    get agences_url
    assert_response :success
  end

  test "should get new" do
    get new_agence_url
    assert_response :success
  end

  test "should create agence" do
    assert_difference('Agence.count') do
      post agences_url, params: { agence: { code: @agence.code, nom: @agence.nom } }
    end

    assert_redirected_to agence_url(Agence.last)
  end

  test "should show agence" do
    get agence_url(@agence)
    assert_response :success
  end

  test "should get edit" do
    get edit_agence_url(@agence)
    assert_response :success
  end

  test "should update agence" do
    patch agence_url(@agence), params: { agence: { code: @agence.code, nom: @agence.nom } }
    assert_redirected_to agence_url(@agence)
  end

  test "should destroy agence" do
    assert_difference('Agence.count', -1) do
      delete agence_url(@agence)
    end

    assert_redirected_to agences_url
  end
end
