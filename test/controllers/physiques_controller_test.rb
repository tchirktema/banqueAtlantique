require 'test_helper'

class PhysiquesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @physique = physiques(:one)
  end

  test "should get index" do
    get physiques_url
    assert_response :success
  end

  test "should get new" do
    get new_physique_url
    assert_response :success
  end

  test "should create physique" do
    assert_difference('Physique.count') do
      post physiques_url, params: { physique: { adehesion_bic: @physique.adehesion_bic, adres_nature: @physique.adres_nature, adresse_residence: @physique.adresse_residence, affichage_sld_si_cond: @physique.affichage_sld_si_cond, agence: @physique.agence, aml: @physique.aml, assujetissement_tva: @physique.assujetissement_tva, autorisation_chequier: @physique.autorisation_chequier, categorie_juridique: @physique.categorie_juridique, classification: @physique.classification, code_ape: @physique.code_ape, code_courier_adresse: @physique.code_courier_adresse, code_sexe: @physique.code_sexe, commune: @physique.commune, commune: @physique.commune, cotation: @physique.cotation, courr: @physique.courr, credit_immed_chq_effets: @physique.credit_immed_chq_effets, date_depart_retraite: @physique.date_depart_retraite, date_echance: @physique.date_echance, date_effet: @physique.date_effet, date_embauche: @physique.date_embauche, date_ouverture: @physique.date_ouverture, delivree: @physique.delivree, dep_de_residence: @physique.dep_de_residence, dep_delivre_ci: @physique.dep_delivre_ci, devise_de_reference: @physique.devise_de_reference, diplome_obtenu: @physique.diplome_obtenu, donne_par: @physique.donne_par, email: @physique.email, employeur: @physique.employeur, etat_civil: @physique.etat_civil, expl_resp: @physique.expl_resp, fatca: @physique.fatca, grp_confid_client: @physique.grp_confid_client, instruction: @physique.instruction, interdiction_chequier: @physique.interdiction_chequier, iso_pays: @physique.iso_pays, iso_pays_naissance: @physique.iso_pays_naissance, iub_cip: @physique.iub_cip, juridique: @physique.juridique, langue: @physique.langue, mand: @physique.mand, marital_nom: @physique.marital_nom, marital_prenom: @physique.marital_prenom, naissance: @physique.naissance, nature_clientele: @physique.nature_clientele, nature_contrat: @physique.nature_contrat, nbre_enfants: @physique.nbre_enfants, nom_de_la_mere: @physique.nom_de_la_mere, nom_du_pere: @physique.nom_du_pere, num_cart_resid: @physique.num_cart_resid, numero: @physique.numero, option_fiscal: @physique.option_fiscal, pays: @physique.pays, pays_adres: @physique.pays_adres, pays_de_nationalite: @physique.pays_de_nationalite, pays_de_resid_fiscal: @physique.pays_de_resid_fiscal, pays_de_residence: @physique.pays_de_residence, pays_de_residence: @physique.pays_de_residence, pays_nationalite: @physique.pays_nationalite, pays_pour_etranger: @physique.pays_pour_etranger, piece: @physique.piece, portable: @physique.portable, pouvoir: @physique.pouvoir, precision_date_de_naissance: @physique.precision_date_de_naissance, prenoms: @physique.prenoms, prenoms_du_mere: @physique.prenoms_du_mere, prenoms_du_pere: @physique.prenoms_du_pere, profession: @physique.profession, qualite: @physique.qualite, racine_cli: @physique.racine_cli, rleve_mois_sans_mvts: @physique.rleve_mois_sans_mvts, salaire_net: @physique.salaire_net, satut_residentiel: @physique.satut_residentiel, secret: @physique.secret, secteur_activite: @physique.secteur_activite, secteur_institutionnel: @physique.secteur_institutionnel, situation_matrimonial: @physique.situation_matrimonial, situation_residentielle: @physique.situation_residentielle, succession: @physique.succession, surveillance: @physique.surveillance, titre: @physique.titre, titre_string: @physique.titre_string, validite: @physique.validite, ville_residence: @physique.ville_residence } }
    end

    assert_redirected_to physique_url(Physique.last)
  end

  test "should show physique" do
    get physique_url(@physique)
    assert_response :success
  end

  test "should get edit" do
    get edit_physique_url(@physique)
    assert_response :success
  end

  test "should update physique" do
    patch physique_url(@physique), params: { physique: { adehesion_bic: @physique.adehesion_bic, adres_nature: @physique.adres_nature, adresse_residence: @physique.adresse_residence, affichage_sld_si_cond: @physique.affichage_sld_si_cond, agence: @physique.agence, aml: @physique.aml, assujetissement_tva: @physique.assujetissement_tva, autorisation_chequier: @physique.autorisation_chequier, categorie_juridique: @physique.categorie_juridique, classification: @physique.classification, code_ape: @physique.code_ape, code_courier_adresse: @physique.code_courier_adresse, code_sexe: @physique.code_sexe, commune: @physique.commune, commune: @physique.commune, cotation: @physique.cotation, courr: @physique.courr, credit_immed_chq_effets: @physique.credit_immed_chq_effets, date_depart_retraite: @physique.date_depart_retraite, date_echance: @physique.date_echance, date_effet: @physique.date_effet, date_embauche: @physique.date_embauche, date_ouverture: @physique.date_ouverture, delivree: @physique.delivree, dep_de_residence: @physique.dep_de_residence, dep_delivre_ci: @physique.dep_delivre_ci, devise_de_reference: @physique.devise_de_reference, diplome_obtenu: @physique.diplome_obtenu, donne_par: @physique.donne_par, email: @physique.email, employeur: @physique.employeur, etat_civil: @physique.etat_civil, expl_resp: @physique.expl_resp, fatca: @physique.fatca, grp_confid_client: @physique.grp_confid_client, instruction: @physique.instruction, interdiction_chequier: @physique.interdiction_chequier, iso_pays: @physique.iso_pays, iso_pays_naissance: @physique.iso_pays_naissance, iub_cip: @physique.iub_cip, juridique: @physique.juridique, langue: @physique.langue, mand: @physique.mand, marital_nom: @physique.marital_nom, marital_prenom: @physique.marital_prenom, naissance: @physique.naissance, nature_clientele: @physique.nature_clientele, nature_contrat: @physique.nature_contrat, nbre_enfants: @physique.nbre_enfants, nom_de_la_mere: @physique.nom_de_la_mere, nom_du_pere: @physique.nom_du_pere, num_cart_resid: @physique.num_cart_resid, numero: @physique.numero, option_fiscal: @physique.option_fiscal, pays: @physique.pays, pays_adres: @physique.pays_adres, pays_de_nationalite: @physique.pays_de_nationalite, pays_de_resid_fiscal: @physique.pays_de_resid_fiscal, pays_de_residence: @physique.pays_de_residence, pays_de_residence: @physique.pays_de_residence, pays_nationalite: @physique.pays_nationalite, pays_pour_etranger: @physique.pays_pour_etranger, piece: @physique.piece, portable: @physique.portable, pouvoir: @physique.pouvoir, precision_date_de_naissance: @physique.precision_date_de_naissance, prenoms: @physique.prenoms, prenoms_du_mere: @physique.prenoms_du_mere, prenoms_du_pere: @physique.prenoms_du_pere, profession: @physique.profession, qualite: @physique.qualite, racine_cli: @physique.racine_cli, rleve_mois_sans_mvts: @physique.rleve_mois_sans_mvts, salaire_net: @physique.salaire_net, satut_residentiel: @physique.satut_residentiel, secret: @physique.secret, secteur_activite: @physique.secteur_activite, secteur_institutionnel: @physique.secteur_institutionnel, situation_matrimonial: @physique.situation_matrimonial, situation_residentielle: @physique.situation_residentielle, succession: @physique.succession, surveillance: @physique.surveillance, titre: @physique.titre, titre_string: @physique.titre_string, validite: @physique.validite, ville_residence: @physique.ville_residence } }
    assert_redirected_to physique_url(@physique)
  end

  test "should destroy physique" do
    assert_difference('Physique.count', -1) do
      delete physique_url(@physique)
    end

    assert_redirected_to physiques_url
  end
end
