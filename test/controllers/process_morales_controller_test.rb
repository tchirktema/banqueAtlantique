require 'test_helper'

class ProcessMoralesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @process_morale = process_morales(:one)
  end

  test "should get index" do
    get process_morales_url
    assert_response :success
  end

  test "should get new" do
    get new_process_morale_url
    assert_response :success
  end

  test "should create process_morale" do
    assert_difference('ProcessMorale.count') do
      post process_morales_url, params: { process_morale: { morale_id: @process_morale.morale_id, statusclient_id: @process_morale.statusclient_id, user_id: @process_morale.user_id } }
    end

    assert_redirected_to process_morale_url(ProcessMorale.last)
  end

  test "should show process_morale" do
    get process_morale_url(@process_morale)
    assert_response :success
  end

  test "should get edit" do
    get edit_process_morale_url(@process_morale)
    assert_response :success
  end

  test "should update process_morale" do
    patch process_morale_url(@process_morale), params: { process_morale: { morale_id: @process_morale.morale_id, statusclient_id: @process_morale.statusclient_id, user_id: @process_morale.user_id } }
    assert_redirected_to process_morale_url(@process_morale)
  end

  test "should destroy process_morale" do
    assert_difference('ProcessMorale.count', -1) do
      delete process_morale_url(@process_morale)
    end

    assert_redirected_to process_morales_url
  end
end
