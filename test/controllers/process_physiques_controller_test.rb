require 'test_helper'

class ProcessPhysiquesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @process_physique = process_physiques(:one)
  end

  test "should get index" do
    get process_physiques_url
    assert_response :success
  end

  test "should get new" do
    get new_process_physique_url
    assert_response :success
  end

  test "should create process_physique" do
    assert_difference('ProcessPhysique.count') do
      post process_physiques_url, params: { process_physique: { morale_id: @process_physique.morale_id, statusclient_id: @process_physique.statusclient_id, user_id: @process_physique.user_id } }
    end

    assert_redirected_to process_physique_url(ProcessPhysique.last)
  end

  test "should show process_physique" do
    get process_physique_url(@process_physique)
    assert_response :success
  end

  test "should get edit" do
    get edit_process_physique_url(@process_physique)
    assert_response :success
  end

  test "should update process_physique" do
    patch process_physique_url(@process_physique), params: { process_physique: { morale_id: @process_physique.morale_id, statusclient_id: @process_physique.statusclient_id, user_id: @process_physique.user_id } }
    assert_redirected_to process_physique_url(@process_physique)
  end

  test "should destroy process_physique" do
    assert_difference('ProcessPhysique.count', -1) do
      delete process_physique_url(@process_physique)
    end

    assert_redirected_to process_physiques_url
  end
end
