class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :lockable
  include DeviseTokenAuth::Concerns::User

  belongs_to :role
  belongs_to :agence
  has_many :process_morales
  has_many :process_physiques


   def as_json(options={})
      super(:only => [:id,:name,:nickname,:email,:created_at,:last_sign_in_at],
          :include =>{
            :agence => {:only => [:nom]},
            :role => {:only => [:titre]}
          }
        )
    end
end
