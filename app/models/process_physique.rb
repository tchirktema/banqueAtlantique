class ProcessPhysique < ApplicationRecord
  belongs_to :user
  belongs_to :statusclient
  belongs_to :physique



  def as_json(options={})
      super(
          :include =>{
          	:only => [:created_at],
            :user => {:only => [:name,:nickname,:email]},
            :statusclient => {:only => [:status]},
            :physique => {:only => [:racine_cli]}
          }
        )
    end
end
