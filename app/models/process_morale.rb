class ProcessMorale < ApplicationRecord
  belongs_to :user
  belongs_to :statusclient
  belongs_to :morale

  def as_json(options={})
      super(
          :include =>{
          	:only => [:created_at],
            :user => {:only => [:name,:nickname,:email]},
            :statusclient => {:only => [:status]},
            :morale => {:only => [:racine_cli]}
          }
        )
    end
end
