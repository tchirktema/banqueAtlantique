json.extract! agence, :id, :code, :nom, :created_at, :updated_at
json.url agence_url(agence, format: :json)
