json.extract! process_physique, :id, :user_id, :statusclient_id, :morale_id, :created_at, :updated_at
json.url process_physique_url(process_physique, format: :json)
