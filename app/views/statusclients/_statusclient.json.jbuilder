json.extract! statusclient, :id, :statut, :created_at, :updated_at
json.url statusclient_url(statusclient, format: :json)
