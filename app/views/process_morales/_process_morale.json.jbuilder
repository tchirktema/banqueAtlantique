json.extract! process_morale, :id, :user_id, :statusclient_id, :morale_id, :created_at, :updated_at
json.url process_morale_url(process_morale, format: :json)
