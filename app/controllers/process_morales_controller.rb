class ProcessMoralesController < ApplicationController
  before_action :set_process_morale, only: [:show, :edit, :update, :destroy]

  # GET /process_morales
  # GET /process_morales.json
  def index
    @process_morales = ProcessMorale.all

    render json: @process_morales
  end

  # GET /process_morales/1
  # GET /process_morales/1.json
  def show
    render json: @process_morale
  end

  # GET /process_morales/new
  def new
    @process_morale = ProcessMorale.new
  end

  # GET /process_morales/1/edit
  def edit
  end

  # POST /process_morales
  # POST /process_morales.json
  def create
    @process_morale = ProcessMorale.new(process_morale_params)

    respond_to do |format|
      if @process_morale.save
        format.html { redirect_to @process_morale, notice: 'Process morale was successfully created.' }
        format.json { render :show, status: :created, location: @process_morale }
      else
        format.html { render :new }
        format.json { render json: @process_morale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /process_morales/1
  # PATCH/PUT /process_morales/1.json
  def update
    # respond_to do |format|
      if @process_morale.update(process_morale_params)
        # format.html { redirect_to @process_morale, notice: 'Process morale was successfully updated.' }
        # format.json { render :show, status: :ok, location: @process_morale }
        render json: @process_morale
      else
        # format.html { render :edit }
        # format.json { render json: @process_morale.errors, status: :unprocessable_entity }
        render json: @process_morale
      end
    # end
  end

  # DELETE /process_morales/1
  # DELETE /process_morales/1.json
  def destroy
    @process_morale.destroy
    # respond_to do |format|
    #   format.html { redirect_to process_morales_url, notice: 'Process morale was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_process_morale
      @process_morale = ProcessMorale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def process_morale_params
      params.require(:process_morale).permit(:user_id, :statusclient_id, :morale_id)
    end
end
