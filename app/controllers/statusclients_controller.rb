class StatusclientsController < ApplicationController
  before_action :set_statusclient, only: [:show, :edit, :update, :destroy]

  # GET /statusclients
  # GET /statusclients.json
  def index
    @statusclients = Statusclient.all

    render json: @statusclients
  end

  # GET /statusclients/1
  # GET /statusclients/1.json
  def show
    render json: @statusclient
  end

  # GET /statusclients/new
  def new
    @statusclient = Statusclient.new
  end

  # GET /statusclients/1/edit
  def edit
  end

  # POST /statusclients
  # POST /statusclients.json
  def create
    @statusclient = Statusclient.new(statusclient_params)

    respond_to do |format|
      if @statusclient.save
        format.html { redirect_to @statusclient, notice: 'Statusclient was successfully created.' }
        format.json { render :show, status: :created, location: @statusclient }
      else
        format.html { render :new }
        format.json { render json: @statusclient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statusclients/1
  # PATCH/PUT /statusclients/1.json
  def update
    respond_to do |format|
      if @statusclient.update(statusclient_params)
        # format.html { redirect_to @statusclient, notice: 'Statusclient was successfully updated.' }
        # format.json { render :show, status: :ok, location: @statusclient }
        render json: @statusclient
      else
        # format.html { render :edit }
        # format.json { render json: @statusclient.errors, status: :unprocessable_entity }
        render json: @statusclient.errors
      end
    end
  end

  # DELETE /statusclients/1
  # DELETE /statusclients/1.json
  def destroy
    @statusclient.destroy
    # respond_to do |format|
    #   format.html { redirect_to statusclients_url, notice: 'Statusclient was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_statusclient
      @statusclient = Statusclient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def statusclient_params
      params.require(:statusclient).permit(:statut)
    end
end
