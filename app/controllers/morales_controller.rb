class MoralesController < ApplicationController
  before_action :set_morale, only: [:show, :edit, :update, :destroy]

  # GET /morales
  # GET /morales.json
  def index
    if params[:search]
        @morales = Morale.where(racine_cli: params[:search])
    end

    render json: @morales
      

  end

  # GET /morales/1
  # GET /morales/1.json
  def show
    render json: @morale
  end

  # GET /morales/new
  def new
    @morale = Morale.new
  end

  # GET /morales/1/edit
  def edit
    
  end

  # POST /morales
  # POST /morales.json
  def create
    @morale = Morale.new(morale_params)

    respond_to do |format|
      if @morale.save
        format.html { redirect_to @morale, notice: 'Morale was successfully created.' }
        format.json { render :show, status: :created, location: @morale }
      else
        format.html { render :new }
        format.json { render json: @morale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /morales/1
  # PATCH/PUT /morales/1.json
  def update
    #respond_to do |format|
      if @morale.update(morale_params)
        #format.html { redirect_to @morale, notice: 'Morale was successfully updated.' }
        #format.json { render :show, status: :ok, location: @morale }
          render json: @morale
      else
        #format.html { render :edit }
        #format.json { render json: @morale.errors, status: :unprocessable_entity }
         render json: @morale.errors
      end
    #end
  end

  # DELETE /morales/1
  # DELETE /morales/1.json
  def destroy
    @morale.destroy
    respond_to do |format|
      format.html { redirect_to morales_url, notice: 'Morale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_morale
      @morale = Morale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def morale_params
      params.require(:morale).permit(:racine_cli, :nom_jur, :date_ouv, :obj_sco, :rcs, :categ_jur, :forme, :pays_crea, :pays_exploit, :code_ape, :lieu, :capital, :ca_social_ht, :adresse_nom, :adresse_comp_nom, :adresse, :ville, :pays, :code_postal, :fixe, :email, :langue, :courr, :prs, :ident, :nom_signataire, :qualite_fonction, :pouvoir, :type_signataire, :dateffet, :date_echance, :mont_max, :date_decision, :bic, :langue, :devise_de_reference, :pays_de_nationalite, :pays_de_residence, :pays_de_residence_fiscal, :public_privee, :option_fiscal, :assujetissement_tva, :actionnaires, :consolide, :apparente, :participation_client_dans_banque, :participatio_banque_dans_client, :autorisation_chequier, :interdiction_chequier, :credit_immed_chq_effets, :contrat_gestion, :calcul_de_performance, :grp_confid_client, :affichage_sld_si_cond, :releves_mois_mvts, :releves_mois_sans_mvts, :classification, :surveillance, :secret, :sucession, :instruction_tel, :date_ouverture, :expl_resp, :agence, :notation_type, :date, :groupe_de_clients_lies, :iub_cip, :nature_clientele, :groupe_activite, :secteur_institutionnel, :forme_juridique, :siren, :ninea, :agec, :secteur_activite, :effect, :sres, :fax, :num_ifu, :segmentation, :fatca, :aml)
    end
end
