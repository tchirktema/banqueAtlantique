class UsersController < ApplicationController
  before_action :set_user, only:[:show,:edit]

  def index
    @users = User.all
    render json: @users
  end

  def show
    #render json: @user
    render :json => {
      :user=>@user,
      :current_sign_in_at => @user.current_sign_in_at,
      :last_sign_in_at => @user.last_sign_in_at,
      :current_sign_in_ip => @user.current_sign_in_ip,
      :last_sign_in_ip => @user.last_sign_in_ip,

      
    }

  end

  def edit

  end

  def new

  end

  def update

  end

  private
    

    def set_user
        @user = User.find(params[:id])
    end
end
