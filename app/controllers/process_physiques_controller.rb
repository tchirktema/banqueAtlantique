class ProcessPhysiquesController < ApplicationController
  before_action :set_process_physique, only: [:show, :edit, :update, :destroy]

  # GET /process_physiques
  # GET /process_physiques.json
  def index
    @process_physiques = ProcessPhysique.all

    render json: @process_physiques
  end

  # GET /process_physiques/1
  # GET /process_physiques/1.json
  def show
    render json: @process_physique
  end

  # GET /process_physiques/new
  def new
    @process_physique = ProcessPhysique.new
  end

  # GET /process_physiques/1/edit
  def edit
  end

  # POST /process_physiques
  # POST /process_physiques.json
  def create
    @process_physique = ProcessPhysique.new(process_physique_params)

    respond_to do |format|
      if @process_physique.save
        format.html { redirect_to @process_physique, notice: 'Process physique was successfully created.' }
        format.json { render :show, status: :created, location: @process_physique }
      else
        format.html { render :new }
        format.json { render json: @process_physique.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /process_physiques/1
  # PATCH/PUT /process_physiques/1.json
  def update
    # respond_to do |format|
      if @process_physique.update(process_physique_params)
        # format.html { redirect_to @process_physique, notice: 'Process physique was successfully updated.' }
        # format.json { render :show, status: :ok, location: @process_physique }
        render json: @process_physique
      else
        # format.html { render :edit }
        # format.json { render json: @process_physique.errors, status: :unprocessable_entity }
        render json: @process_physique
      end
    # end
  end

  # DELETE /process_physiques/1
  # DELETE /process_physiques/1.json
  def destroy
    @process_physique.destroy
    # respond_to do |format|
    #   format.html { redirect_to process_physiques_url, notice: 'Process physique was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_process_physique
      @process_physique = ProcessPhysique.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def process_physique_params
      params.require(:process_physique).permit(:user_id, :statusclient_id, :physique_id)
    end
end
