class PhysiquesController < ApplicationController
  before_action :set_physique, only: [:show, :edit, :update, :destroy]

  # GET /physiques
  # GET /physiques.json
  def index
    if params[:search]
        @physiques = Physique.where(racine_cli: params[:search])
    end

    render json: @physiques
  end

  # GET /physiques/1
  # GET /physiques/1.json
  def show
    render json: @physique
  end

  # GET /physiques/new
  def new
    @physique = Physique.new
  end

  # GET /physiques/1/edit
  def edit
  end

  # POST /physiques
  # POST /physiques.json
  def create
    @physique = Physique.new(physique_params)

    respond_to do |format|
      if @physique.save
        format.html { redirect_to @physique, notice: 'Physique was successfully created.' }
        format.json { render :show, status: :created, location: @physique }
      else
        format.html { render :new }
        format.json { render json: @physique.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /physiques/1
  # PATCH/PUT /physiques/1.json
  def update
    #respond_to do |format|
      if @physique.update(physique_params)
        #format.html { redirect_to @physique, notice: 'Physique was successfully updated.' }
        #format.json { render :show, status: :ok, location: @physique }
        render json: @physique
      else
        #format.html { render :edit }
        #format.json { render json: @physique.errors, status: :unprocessable_entity }
        render json: @physique.errors
      end
    #end
  end

  # DELETE /physiques/1
  # DELETE /physiques/1.json
  def destroy
    @physique.destroy
    # respond_to do |format|
    #   format.html { redirect_to physiques_url, notice: 'Physique was successfully destroyed.' }
    #   format.json { head :no_content }
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_physique
      @physique = Physique.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def physique_params
      params.require(:physique).permit(:racine_cli, :etat_civil, :code_sexe, :situation_matrimonial, :juridique, :qualite, :profession, :nbre_enfants, :prenoms, :marital_nom, :marital_prenom, :nom_de_la_mere, :prenoms_du_mere, :nom_du_pere, :prenoms_du_pere, :piece, :numero, :iso_pays, :dep_delivre_ci, :delivree, :validite, :pays_nationalite, :naissance, :iso_pays_naissance, :commune, :pays_de_residence, :dep_de_residence, :adresse_residence, :ville_residence, :pays, :portable, :courr, :diplome_obtenu, :salaire_net, :date_embauche, :date_depart_retraite, :employeur, :situation_residentielle, :nature_contrat, :adehesion_bic, :adres_nature, :pays_adres, :code_courier_adresse, :commune, :pays_pour_etranger, :langue, :devise_de_reference, :pays_de_nationalite, :pays_de_residence, :pays_de_resid_fiscal, :categorie_juridique, :code_ape, :option_fiscal, :assujetissement_tva, :credit_immed_chq_effets, :grp_confid_client, :affichage_sld_si_cond, :rleve_mois_sans_mvts, :classification, :surveillance, :secret, :succession, :instruction, :date_ouverture, :expl_resp, :agence, :autorisation_chequier, :interdiction_chequier, :num_cart_resid, :iub_cip, :secteur_institutionnel, :titre_string, :precision_date_de_naissance, :nature_clientele, :secteur_activite, :mand, :donne_par, :pouvoir, :date_effet, :date_echance, :email, :cotation, :titre, :satut_residentiel, :fatca, :aml)
    end
end
