class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
  #protect_from_forgery with: :exception
  protect_from_forgery with: :exception, if: Proc.new { |c| c.request.format != 'application/json' }
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_action :configure_permitted_parameters, if: :devise_controller?


  protected

 def configure_permitted_parameters
   devise_parameter_sanitizer.permit :sign_up, keys:  [:name]
   #update_attrs = [ :id, :name, :nickname,:provider, :uid, :image, :created_at, :updated_at, :superbanque, :collecteur, :confirmation]
   devise_parameter_sanitizer.permit :account_update, keys:  [ :id, :name, :nickname,:provider, :uid, :image, :created_at, :updated_at,:role_id,:agence_id,:last_sign_in_at,:agence, :role, :registration
]

   #devise_parameter_sanitizer.for(:account_update).push()
 end
end
