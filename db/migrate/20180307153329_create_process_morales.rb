class CreateProcessMorales < ActiveRecord::Migration[5.1]
  def change
    create_table :process_morales do |t|
      t.references :user, foreign_key: true
      t.references :statusclient, foreign_key: true
      t.references :morale, foreign_key: true

      t.timestamps
    end
  end
end
