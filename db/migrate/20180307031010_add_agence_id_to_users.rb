class AddAgenceIdToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :agence_id, :integer, default: 1
  end
end
