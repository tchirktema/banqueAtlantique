class CreateAgences < ActiveRecord::Migration[5.1]
  def change
    create_table :agences do |t|
      t.string :code
      t.string :nom

      t.timestamps
    end
  end
end
