class CreateStatusclients < ActiveRecord::Migration[5.1]
  def change
    create_table :statusclients do |t|
      t.string :statut

      t.timestamps
    end
  end
end
