class CreateProcessPhysiques < ActiveRecord::Migration[5.1]
  def change
    create_table :process_physiques do |t|
      t.references :user, foreign_key: true
      t.references :statusclient, foreign_key: true
      t.references :physique, foreign_key: true

      t.timestamps
    end
  end
end
