# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180307154050) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agences", force: :cascade do |t|
    t.string "code"
    t.string "nom"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "morales", force: :cascade do |t|
    t.string "racine_cli"
    t.string "nom_jur"
    t.string "date_ouv"
    t.string "obj_sco"
    t.string "rcs"
    t.string "categ_jur"
    t.string "forme"
    t.string "pays_crea"
    t.string "pays_exploit"
    t.string "code_ape"
    t.string "lieu"
    t.string "capital"
    t.string "ca_social_ht"
    t.string "adresse_nom"
    t.string "adresse_comp_nom"
    t.string "adresse"
    t.string "ville"
    t.string "pays"
    t.string "code_postal"
    t.string "fixe"
    t.string "email"
    t.string "langue"
    t.string "courr"
    t.string "prs"
    t.string "ident"
    t.string "nom_signataire"
    t.string "qualite_fonction"
    t.string "pouvoir"
    t.string "type_signataire"
    t.string "dateffet"
    t.string "date_echance"
    t.string "mont_max"
    t.string "date_decision"
    t.string "bic"
    t.string "devise_de_reference"
    t.string "pays_de_nationalite"
    t.string "pays_de_residence"
    t.string "pays_de_residence_fiscal"
    t.string "public_privee"
    t.string "option_fiscal"
    t.string "assujetissement_tva"
    t.string "actionnaires"
    t.string "consolide"
    t.string "apparente"
    t.string "participation_client_dans_banque"
    t.string "participatio_banque_dans_client"
    t.string "autorisation_chequier"
    t.string "interdiction_chequier"
    t.string "credit_immed_chq_effets"
    t.string "contrat_gestion"
    t.string "calcul_de_performance"
    t.string "grp_confid_client"
    t.string "affichage_sld_si_cond"
    t.string "releves_mois_mvts"
    t.string "releves_mois_sans_mvts"
    t.string "classification"
    t.string "surveillance"
    t.string "secret"
    t.string "sucession"
    t.string "instruction_tel"
    t.string "date_ouverture"
    t.string "expl_resp"
    t.string "agence"
    t.string "notation_type"
    t.string "date"
    t.string "groupe_de_clients_lies"
    t.string "iub_cip"
    t.string "nature_clientele"
    t.string "groupe_activite"
    t.string "secteur_institutionnel"
    t.string "forme_juridique"
    t.string "siren"
    t.string "ninea"
    t.string "agec"
    t.string "secteur_activite"
    t.string "effect"
    t.string "sres"
    t.string "fax"
    t.string "num_ifu"
    t.string "segmentation"
    t.string "fatca"
    t.string "aml"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "physiques", force: :cascade do |t|
    t.string "racine_cli"
    t.string "etat_civil"
    t.string "code_sexe"
    t.string "situation_matrimonial"
    t.string "juridique"
    t.string "qualite"
    t.string "profession"
    t.string "nbre_enfants"
    t.string "prenoms"
    t.string "marital_nom"
    t.string "marital_prenom"
    t.string "nom_de_la_mere"
    t.string "prenoms_du_mere"
    t.string "nom_du_pere"
    t.string "prenoms_du_pere"
    t.string "piece"
    t.string "numero"
    t.string "iso_pays"
    t.string "dep_delivre_ci"
    t.string "delivree"
    t.string "validite"
    t.string "pays_nationalite"
    t.string "naissance"
    t.string "iso_pays_naissance"
    t.string "commune"
    t.string "pays_de_residence"
    t.string "dep_de_residence"
    t.string "adresse_residence"
    t.string "ville_residence"
    t.string "pays"
    t.string "portable"
    t.string "courr"
    t.string "diplome_obtenu"
    t.string "salaire_net"
    t.string "date_embauche"
    t.string "date_depart_retraite"
    t.string "employeur"
    t.string "situation_residentielle"
    t.string "nature_contrat"
    t.string "adehesion_bic"
    t.string "adres_nature"
    t.string "pays_adres"
    t.string "code_courier_adresse"
    t.string "pays_pour_etranger"
    t.string "langue"
    t.string "devise_de_reference"
    t.string "pays_de_nationalite"
    t.string "pays_de_resid_fiscal"
    t.string "categorie_juridique"
    t.string "code_ape"
    t.string "option_fiscal"
    t.string "assujetissement_tva"
    t.string "credit_immed_chq_effets"
    t.string "grp_confid_client"
    t.string "affichage_sld_si_cond"
    t.string "rleve_mois_sans_mvts"
    t.string "classification"
    t.string "surveillance"
    t.string "secret"
    t.string "succession"
    t.string "instruction"
    t.string "date_ouverture"
    t.string "expl_resp"
    t.string "agence"
    t.string "autorisation_chequier"
    t.string "interdiction_chequier"
    t.string "num_cart_resid"
    t.string "iub_cip"
    t.string "secteur_institutionnel"
    t.string "titre_string"
    t.string "precision_date_de_naissance"
    t.string "nature_clientele"
    t.string "secteur_activite"
    t.string "mand"
    t.string "donne_par"
    t.string "pouvoir"
    t.string "date_effet"
    t.string "date_echance"
    t.string "email"
    t.string "cotation"
    t.string "titre"
    t.string "satut_residentiel"
    t.string "fatca"
    t.string "aml"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "process_morales", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "statusclient_id"
    t.bigint "morale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["morale_id"], name: "index_process_morales_on_morale_id"
    t.index ["statusclient_id"], name: "index_process_morales_on_statusclient_id"
    t.index ["user_id"], name: "index_process_morales_on_user_id"
  end

  create_table "process_physiques", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "statusclient_id"
    t.bigint "physique_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["physique_id"], name: "index_process_physiques_on_physique_id"
    t.index ["statusclient_id"], name: "index_process_physiques_on_statusclient_id"
    t.index ["user_id"], name: "index_process_physiques_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "titre"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "statusclients", force: :cascade do |t|
    t.string "statut"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role_id", default: 1
    t.integer "agence_id", default: 1
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "process_morales", "morales"
  add_foreign_key "process_morales", "statusclients"
  add_foreign_key "process_morales", "users"
  add_foreign_key "process_physiques", "physiques"
  add_foreign_key "process_physiques", "statusclients"
  add_foreign_key "process_physiques", "users"
end
