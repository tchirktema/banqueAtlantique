Rails.application.routes.draw do


  
  resources :process_physiques
  resources :process_morales
  resources :physiques
  mount RailsAdmin::Engine => '/banqueopenadmin', as: 'rails_admin'
  resources :morales
  resources :statusclients
  resources :agences
  resources :roles
  mount_devise_token_auth_for 'User', at: 'auth'
  resources :users, only:[:index,:show,:edit,:new]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
